const fs = require('fs')
const path = require('path')

const filepath = path.join(__dirname, 'input.txt')
fs.readFile(filepath, {encoding: 'utf-8'}, (err, data) => {
	const lines = data.split('\n')

	const resultPart1 = part1(lines)
	console.debug('part 1', resultPart1)

	const resultPart2 = part2(lines)
	console.debug('part 2', resultPart2)
})

function part1(lines) {
	let total = 0
	for (let line of lines) {
		const value = parseInt(line)
		if (!isNaN(value)) {
			total += value
		}
	}

	return total
}

function part2(lines) {
	let total = 0
	let frequencies = {
		0: true
	}

	while (true) {
		for (let line of lines) {
			const value = parseInt(line)
			total += value
			if (frequencies[total] !== undefined) {
				return total
			}

			frequencies[total] = true
		}
	}
}
