const fs = require('fs')
const path = require('path')

const filepath = path.join(__dirname, 'input.txt')
fs.readFile(filepath, {encoding: 'utf-8'}, (err, data) => {
	const lines = data.split('\n')

	const resultPart1 = part1(lines)
	console.debug('part 1', resultPart1)

	// const resultPart2 = part2(lines)
	// console.debug('part 2', resultPart2)
})

function part1(lines) {
	let nbTwo = 0
	let nbThree = 0

	for (let line of lines) {
		const occurences = getOccurencesCount(line)
		
		if (isCharFoundTimes(occurences, 2)) {
			nbTwo++
		}

		if (isCharFoundTimes(occurences, 3)) {
			nbThree++
		}
	}

	return nbTwo * nbThree
}

function getOccurencesCount(line) {
	const occurences = {}
	for (let i = 0; i < line.length; i++) {
		const char = line[i]
		if (occurences[char] === undefined) {
			occurences[char] = 0
		}
		
		occurences[char]++
	}

	return occurences
}

function isCharFoundTimes(occurences, times) {
	for (const letter in occurences) {
		if (occurences[letter] === times) {
			return true
		}
	}

	return false
}

function part2(lines) {
}
